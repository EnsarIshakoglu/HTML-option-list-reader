import re
import json

lines = []
option_texts = []
pattern = ">(.*)</"

# Read file
with open('html_option_values.txt') as f:
    lines = f.readlines()

# Retrieve value of the option text 
for line in lines:
    option_text = re.search(pattern, line).group(1)
    option_texts.append(option_text)

# Create file with values
new_file = open("option_values.txt", "w+")
new_file.writelines(json.dumps(option_texts))